﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoProject.Application.Services.Products;
using DemoProject.Application.Services.Products.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace DemoProject.Presentatin.Controllers
{
    public class ProductsController : BaseController
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        public IActionResult Index(ProductSearchDTO productSearchDTO)
        {
            IEnumerable<ProductDTO> productDTOs = _productService.FindList(productSearchDTO);
            return View(productDTOs);
        }

        public IActionResult ProductDetails(int id)
        {
            ProductUpdateDTO productUpdateDTO = _productService.FindById(id);
            return View(productUpdateDTO);
        }

        public IActionResult Insert(ProductInsertDTO productInsertDTO)
        {
            _productService.Insert(productInsertDTO);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Update(ProductUpdateDTO productUpdateDTO)
        {
            _productService.Update(productUpdateDTO);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Delete(int id)
        {
            _productService.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}