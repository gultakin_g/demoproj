﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProject.Domain.Models.Products
{
    public class ProductSearch
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
