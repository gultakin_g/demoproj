﻿using System.ComponentModel.DataAnnotations;

namespace DemoProject.Domain.Models.Bases
{
    public class BaseEntity 
    {
        [Key]
        public int Id { get; set; }
    }
}
