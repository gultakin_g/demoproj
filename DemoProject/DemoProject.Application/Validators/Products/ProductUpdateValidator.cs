﻿using DemoProject.Application.Services.Products.DTOs;
using FluentValidation;

namespace DemoProject.Application.Validators.Products
{
    public class ProductUpdateValidator : AbstractValidator<ProductUpdateDTO>
    {
        public ProductUpdateValidator()
        {
            RuleFor(e => e.Id).GreaterThan(0).WithMessage("Id is required");
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required");
        }
    }
}
