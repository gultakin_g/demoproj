﻿
using DemoProject.Application.Services.Products.DTOs;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoProject.Application.Validators.Products
{
    public class ProductInsertValidator : AbstractValidator<ProductInsertDTO>
    {
        public ProductInsertValidator()
        {
            RuleFor(e => e.Name).NotNull().NotEmpty().WithMessage("Name is required!");
        }
    }
}
