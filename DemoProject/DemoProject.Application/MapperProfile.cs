﻿using AutoMapper;
using DemoProject.Application.Services.Products.DTOs;
using DemoProject.Domain.Models.Products;

namespace DemoProject.Application
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            MapProducts();
        }

        private void MapProducts()
        {
            CreateMap<ProductSearchDTO, ProductSearch>();
            CreateMap<ProductCheckExistenceDTO, Product>();
            CreateMap<ProductInsertDTO, Product>();
            CreateMap<ProductUpdateDTO, Product>().ReverseMap();
            CreateMap<Product, ProductDTO>();
        }
    }
}
