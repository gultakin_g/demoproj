﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoProject.Application.Services.Products.DTOs
{
    public class ProductInsertDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
