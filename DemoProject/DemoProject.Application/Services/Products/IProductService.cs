﻿using DemoProject.Application.Services.Products.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoProject.Application.Services.Products
{
    public interface IProductService
    {
        IEnumerable<ProductDTO> FindList(ProductSearchDTO productSearchDTO);
        ProductUpdateDTO FindById(int id);
        bool CheckExistence(ProductCheckExistenceDTO productCheckExistenceDTO);
        void Insert(ProductInsertDTO productInsertDTO);
        void Update(ProductUpdateDTO productUpdateDTO);
        void Delete(int id);
    }
}
