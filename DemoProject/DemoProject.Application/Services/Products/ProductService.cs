﻿using AutoMapper;
using DemoProject.Application.Services.Products.DTOs;
using DemoProject.Domain.Models.Products;
using DemoProject.Infrastructure.Repositories.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoProject.Application.Services.Products
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public ProductService(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        public IEnumerable<ProductDTO> FindList(ProductSearchDTO productSearchDTO)
        {
            ProductSearch productSearch = _mapper.Map<ProductSearch>(productSearchDTO);
            IEnumerable<Product> products = _productRepository.FindList(productSearch);
            IEnumerable<ProductDTO> productDTO = _mapper.Map<IEnumerable<ProductDTO>>(products);
            return productDTO;
        }
       
        public ProductUpdateDTO FindById(int id)
        {
            Product product = _productRepository.FindById(id);
            ProductUpdateDTO productUpdateDTO = _mapper.Map<ProductUpdateDTO>(product);
            return productUpdateDTO;
        }

        public bool CheckExistence(ProductCheckExistenceDTO productCheckExistenceDTO)
        {
            Product product = _mapper.Map<Product>(productCheckExistenceDTO);
            bool result = _productRepository.CheckExistence(product);
            return result;
        }


        public void Insert(ProductInsertDTO productInsertDTO)
        {
            Product product = _mapper.Map<Product>(productInsertDTO);
            _productRepository.Insert(product);
        }

        public void Update(ProductUpdateDTO productUpdateDTO)
        {
            Product product = _mapper.Map<Product>(productUpdateDTO);
            _productRepository.Update(product);
        }

        public void Delete(int id)
        {
            _productRepository.Delete(id);
        }
    }
}
