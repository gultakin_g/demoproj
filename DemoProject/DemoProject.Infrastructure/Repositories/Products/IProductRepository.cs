﻿using DemoProject.Domain.Models.Products;
using System.Collections.Generic;

namespace DemoProject.Infrastructure.Repositories.Products
{
    public interface IProductRepository
    {
        IEnumerable<Product> FindList(ProductSearch productSearch);
        Product FindById(int id);
        bool CheckExistence(Product product);
        void Insert(Product product);
        void Update(Product product);
        void Delete(int id);
    }
}
