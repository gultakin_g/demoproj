﻿using DemoProject.Domain.Models.Products;
using System.Collections.Generic;
using System.Linq;

namespace DemoProject.Infrastructure.Repositories.Products
{
    public class ProductRepository : IProductRepository
    {
        private readonly ProductContext _productContext;

        public ProductRepository(ProductContext productContext)
        {
            _productContext = productContext;
        }

        public IEnumerable<Product> FindList(ProductSearch productSearch)
        {
            IEnumerable<Product> products = _productContext.Products
                .Where(e => productSearch.Name == null || e.Name.Contains(productSearch.Name)) 
                .Where(e => productSearch.Description == null || e.Description.Contains(productSearch.Description))
                .AsEnumerable();
            return products;
        }

        public Product FindById(int id)
        {
            Product product = _productContext.Products.FirstOrDefault(e => e.Id == id);
            return product;
        }

        public bool CheckExistence(Product product)
        {
            Product entity = _productContext.Products.FirstOrDefault(e => e.Name == product.Name);
            
            if (entity == null)
            {
                return false;
            }

            if (entity.Id != product.Id)
            {
                return true;
            }

            return false;
        }

        public void Insert(Product product)
        {
            _productContext.Products.Add(product);
            _productContext.SaveChanges();
        }

        public void Update(Product product)
        {
            _productContext.Products.Update(product);
            _productContext.SaveChanges();
        }

        public void Delete(int id)
        {
            Product product = _productContext.Products.First(e => e.Id == id);
            _productContext.Products.Remove(product);
            _productContext.SaveChanges();
        }
    }
}
